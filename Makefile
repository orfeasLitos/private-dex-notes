#.ONESHELL:
private-dex-notes.pdf: private-dex-notes.tex
	pdflatex --shell-escape -halt-on-error -interaction=nonstopmode private-dex-notes.tex; \
	bibtex private-dex-notes.aux; \
	pdflatex --shell-escape -halt-on-error -interaction=nonstopmode private-dex-notes.tex; \
	pdflatex --shell-escape -halt-on-error -interaction=nonstopmode private-dex-notes.tex; \
	rm -rf private-dex-notes.aux private-dex-notes.log private-dex-notes.out private-dex-notes.bbl private-dex-notes.blg

clean:
	rm -rf *.aux *.log *.out *.pdf *.bbl *.blg
